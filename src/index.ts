interface Param {
  number: number
}

type CreateParam = (param: number) => Param
type AddParam = (param1: Param, param2: Param) => string

export const createParam: CreateParam = (number) => {
  return {
    number
  }
}

export const addParam: AddParam = (a, b) => {
  const result = a.number + b.number
  return result.toString()
}
